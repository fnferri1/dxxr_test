# https://medium.com/@jessgreb01/how-to-terraform-locking-state-in-s3-2dc9a5665cb6

# s3 bucket
resource "aws_s3_bucket" "tfstate-s3" {
  bucket = var.bucket_name

  versioning {
    enabled = true
  }

  lifecycle {
    prevent_destroy = true
  }

  tags = merge(
    {
      Name = "tfstate-s3",
    },
    var.tags,
  )
}

# tfstate lock
resource "aws_dynamodb_table" "tfstate-lock" {
  name           = var.dynamodb_table_name
  hash_key       = "LockID"
  read_capacity  = 20
  write_capacity = 20

  attribute {
    name = "LockID"
    type = "S"
  }

  tags = merge(
    {
      Name = "tfstate-lock",
    },
    var.tags,
  )
}
